# Title: Analytical solution to determine IQR based on number of duplicates in wet chemistry dataset
# Date: March 5, 2021
# Authors: C.C.E. van Leeuwen, V.L. Mulder, N.H. Batjes, G.B.M. Heuvelink

rm(list = ls()) 
#setwd()            # please change to your wd

sigmasq = 0.4       # Sigma of data
k=500               # Number of duplicate measurements

q1 = qchisq(0.25,k) # First quartile, 25%
q3 = qchisq(0.75,k) # Third quartile, 75%

IQR = (sigmasq/k)*(q3 - q1) # Calculate interquartile range
IQR