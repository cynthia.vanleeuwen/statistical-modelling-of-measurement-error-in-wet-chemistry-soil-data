# Statistical modelling of measurement error - repo
This repository includes the simplified R scripts used in the manuscript "Statistical modelling of measurement error in wet chemistry soil data", submitted to the European Journal of Soil Science on March 5, 2021.

# Authors: 
C.C.E. van Leeuwen (1,2), V.L. Mulder (2), N.H. Batjes (1), and G.B.M. Heuvelink (1,2)

1. ISRIC World Soil Information, PO Box 353, 6700 AJ Wageningen, the Netherlands
2. Soil Geography and Landscape Group, Wageningen University, PO Box 47, 6700AA Wageningen, the Netherlands

# Content:
1. Analytical tool to estimate IQR based on number of duplicate measurements.
2. Linear mixed-effects model - applied on syntethic balanced data.
3. Linear mixed-effects model - applied on synthetic unbalanced data.
